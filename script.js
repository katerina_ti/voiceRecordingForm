var questions;
var xhr = new XMLHttpRequest();
var emailContainer = document.querySelector(".emailContainer");


var search = new URLSearchParams(location.search);
xhr.open("GET", "https://shrouded-tor-63575.herokuapp.com/api/question_lists?token=" + search.get('token'));
xhr.setRequestHeader("X-Token", "GiFI8pt4WaFB9w==");
xhr.onload = function () {
  try {
    questions = JSON.parse(xhr.response);
    if (questions && Array.isArray(questions)) {
      init();
    } else {
      emailContainer.innerText = "Wrong URL. Sorry no question for this interview.";
    }
  }
  catch (e) {
    emailContainer.innerText = "Wrong URL. Sorry no question for this interview.";

  }
};
  xhr.send();



function init() {
  var dataToSend = {
    answers: [],
    token: search.get("token")
  };
  var switchContainer = document.querySelector(".switchContainer");
  var emailField = document.querySelector(".emailField");
  var voiceRecordingScreen = document.querySelector(".voiceRecordingScreen");
  var clickers = document.querySelectorAll(".clicker");
  var returnContainer = document.querySelector(".returnContainer");
  var emailContainer = document.querySelector(".emailContainer");
  var letsGoContainer = document.querySelector(".letsGoContainer");
  var lastContainer = document.querySelector(".lastContainer");


  emailField.addEventListener("keyup", redisableNextButton);

  Array.prototype.forEach.call(clickers, function (clicker) {
    clicker.addEventListener("click", function () {
      var emailData = emailField.value;
      if (clicker.getAttribute("id") == "sendEmailButton") {
        var xhrEmail = new XMLHttpRequest();
        xhrEmail.open("POST", "https://shrouded-tor-63575.herokuapp.com/api/users?user_type=candidate&email=" + emailData );
        xhrEmail.setRequestHeader("X-Token", "GiFI8pt4WaFB9w==");
        xhrEmail.onload = function () {
          var parsedResponse = JSON.parse(xhrEmail.response);
          if(xhrEmail.status == 422) {
            hide(emailContainer);
            hide(letsGoContainer);
            show(returnContainer);
          } else {
            var hidedSelector = clicker.getAttribute("data-hide");
            if (hidedSelector){
              var hidedEl = document.querySelector(hidedSelector);
              hide(hidedEl)
            }
            var showSelector = clicker.getAttribute("data-show");
            if (showSelector) {
              var showEl = document.querySelector(showSelector);
              show(showEl)
            }
          }
        };
        xhrEmail.send();
      } else {
        var hidedSelector = clicker.getAttribute("data-hide");
        if (hidedSelector){
          var hidedEl = document.querySelector(hidedSelector);
          hide(hidedEl)
        }
        var showSelector = clicker.getAttribute("data-show");
        if (showSelector) {
          var showEl = document.querySelector(showSelector);
          show(showEl)
        }
      }
    })
  });

  function validateEmail(email) {
    console.log(email);
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email.toLowerCase());
  }

  function redisableNextButton(e) {
    dataToSend.email = emailField.value;
    console.log(dataToSend.email);
    // var emailFieldCondition = emailField.checkValidity();
    if (validateEmail(dataToSend.email)) {
      switchContainer.disabled = false;
    } else {
      switchContainer.disabled = true;
    }
  }

  var threeMinutes = 60 * 3 - 1;
  var countDown = document.querySelector(".countDown");
  var intervalId;

  var startButton = document.querySelector(".startButton");
  var finishButton = document.querySelector(".finishButton");
  var repeatButton = document.querySelector(".repeatButton");
  var submitButton = document.querySelector(".submitButton");

  startButton.addEventListener("click", function () {
    currentQuestion.status = 'recording';
    renderQuestion(currentQuestion, questions.length);

    startTimer(threeMinutes, countDown);
  });
  finishButton.addEventListener("click", function () {
    currentQuestion.status = 'recorded';
    renderQuestion(currentQuestion, questions.length);

    clearInterval(intervalId);
    show(nextQuestionButton);

  });
  repeatButton.addEventListener("click", function () {
    currentQuestion.status = 'recording';
    renderQuestion(currentQuestion, questions.length);

    clearInterval(intervalId);
    startTimer(threeMinutes, countDown);
  });
  var submittingData = document.querySelector(".submittingData");
  var endContainer = document.querySelector(".endContainer");
  submitButton.addEventListener("click", function () {
    var xhr = new XMLHttpRequest();
    xhr.open("POST","https://shrouded-tor-63575.herokuapp.com/api/answers/upload");
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.setRequestHeader('X-Token', 'GiFI8pt4WaFB9w==');
    xhr.send(JSON.stringify(dataToSend));
    hide(voiceRecordingScreen);
    show(submittingData);
    xhr.onload = function () {
      hide(submittingData);
      show(endContainer);
      hide(voiceRecordingScreen);
    }
  });
  function show(el) {
    if (el) {
      el.classList.remove("hidden");
    }
  }

  function hide(el) {
    if (el) {
      el.classList.add("hidden");
    }
  }

  var containers = document.querySelectorAll(".container");
  var currentContainerIndex = 0;
  var currentContainer = containers[currentContainerIndex];

  document.addEventListener("click", function (e) {
    if (e.target.classList.contains('next')) {
      hide(currentContainer);
      currentContainerIndex++;
      currentContainer = containers[currentContainerIndex];
      show(currentContainer);
    }
  });

  var currentQuestionIndex = 0;
  var currentQuestion = questions[currentQuestionIndex];
  var lastQuestionIndex = parseInt(questions[0].total) - 1;
  var startOrder = document.querySelector(".startOrder");
  var endOrder = document.querySelector(".endOrder");
  var questionHeader = document.querySelector(".questionHeader");

  function renderQuestion(question, questionsAmount) {
    startOrder.innerText = question.order;
    endOrder.innerText = questionsAmount;
    // questionHeader.innerText = question.text;
    voiceRecordingScreen.setAttribute('status', question.status || 'not-started');
    if (currentQuestionIndex === questionsAmount - 1) {
      voiceRecordingScreen.setAttribute('last', 'true');
    } else {
      voiceRecordingScreen.setAttribute('last', 'false');
    }
  }

  renderQuestion(questions[currentQuestionIndex], questions.length);

  var nextQuestionButton = document.querySelector(".nextButton");
  nextQuestionButton.addEventListener("click", function () {
    currentQuestionIndex++;
    currentQuestion = questions[currentQuestionIndex];

    show(previousQuestionButton);
    renderQuestion(questions[currentQuestionIndex], questions.length);
    if (currentQuestionIndex == lastQuestionIndex) {
      hide(nextQuestionButton);
    }
    countDown.innerText = "03:00";
  });


  var previousQuestionButton = document.querySelector(".previousButton");
  previousQuestionButton.addEventListener("click", function () {
    currentQuestionIndex--;
    currentQuestion = questions[currentQuestionIndex];

    show(nextQuestionButton);
    renderQuestion(questions[currentQuestionIndex], questions.length);
    if (currentQuestionIndex == 0) {
      hide(previousQuestionButton);
    }
    countDown.innerText = "03:00";
  });


  function startTimer(duration, display) {
    var timer = duration, minutes, seconds;
    intervalId = setInterval(function () {
      minutes = parseInt(timer / 60, 10);
      seconds = parseInt(timer % 60, 10);

      minutes = minutes < 10 ? "0" + minutes : minutes;
      seconds = seconds < 10 ? "0" + seconds : seconds;

      display.innerText = minutes + ":" + seconds;

      if (--timer < 0) {
        currentQuestion.status = 'recorded';
        renderQuestion(currentQuestion, questions.length);

        clearInterval(intervalId);
        show(nextQuestionButton);
        mediaRecorder.stop();
      }
    }, 1000);
  }

  var mediaRecorder;
  var notAvailable = document.querySelector(".notAvailable");
  var contactContainer = document.querySelector(".contactContainer");
  var clickerIos = document.querySelector(".clickerIos");

  if (!('mediaDevices' in navigator) || !('MediaRecorder' in window)) {

    clickerIos.addEventListener("click", function () {
      var hidedSelector = clickerIos.getAttribute("data-hide-ios");
      if (hidedSelector){
        var hidedEl = document.querySelector(hidedSelector);
        hide(hidedEl)
      }
      var showSelector = clickerIos.getAttribute("data-show-ios");
      if (showSelector) {
        var showEl = document.querySelector(showSelector);
        show(showEl)
      }
    })
  } else {
    navigator.mediaDevices.getUserMedia({audio: true, video: false})
      .then(function (mediaStream) {
        var chunks = [];
        mediaRecorder = new MediaRecorder(mediaStream);

        startButton.addEventListener("click", function () {
          mediaRecorder.start();
        });

        finishButton.addEventListener("click", function () {
          mediaRecorder.stop();
        });

        repeatButton.addEventListener("click", function () {
          mediaRecorder.start();
        });

        mediaRecorder.ondataavailable = function (e) {
          chunks.push(e.data);
        };
        mediaRecorder.onstop = function () {
          var blob = new Blob(chunks, {'type': 'audio/ogg; codecs=opus'});

          var reader = new window.FileReader();
          reader.readAsDataURL(blob);
          reader.onloadend = function () {
            dataToSend.answers[currentQuestionIndex] = {
              answer_audio: reader.result,
              question_id: questions[currentQuestionIndex].id
            };
            chunks = [];
            console.log(questions.length, dataToSend.answers.length);
            if(questions.length == dataToSend.answers.length) {
              show(submitButton);
            }

            console.log(dataToSend);
          };
        }
      });
  }
}
// function onFbLoaded(details) {
//   checkLoginState(function (fbData) {
//     console.log(fbData);
//     if (fbData.authResponse) {
//       dataToSend.facebook_connect = fbData.authResponse
//     }
//   });
// }

//
// function checkLoginState(cb) {
//   FB.getLoginStatus(function (response) {
//     cb(response);
//   });
// }


// window.fbAsyncInit = function () {
//   FB.init({
// //      appId: '1966593120284386',
//     appId: '1128879343912771',
//     autoLogAppEvents: true,
//     xfbml: true,
//     version: 'v2.10'
//   });
//   FB.AppEvents.logPageView();
//
//
//   FB.Event.subscribe('auth.authResponseChange', function (response) {
//     if (response.status === 'connected') {
//       console.log("<br>Connected to Facebook");
//       //SUCCESS
//       FB.api('/me', {locale: 'en_EN', fields: 'name, email'},
//         function (response) {
//           console.log(response);
//           emailField.value = response.email;
//           dataToSend.email = response.email;
//           dataToSend.name = response.name;
//           switchContainer.disabled = false;
//         }
//       );
//
//     }
//     else if (response.status === 'not_authorized') {
//       console.log("Failed to Connect");
//
//       //FAILED
//     } else {
//       console.log("Logged Out");
//
//       //UNKNOWN ERROR
//     }
//   });
//
// };


// (function (d, s, id) {
//   var js, fjs = d.getElementsByTagName(s)[0];
//   if (d.getElementById(id)) {
//     return;
//   }
//   js = d.createElement(s);
//   js.id = id;
//   js.src = "//connect.facebook.net/en_US/sdk.js";
//   fjs.parentNode.insertBefore(js, fjs);
// }(document, 'script', 'facebook-jssdk'));